#!/bin/bash

# $1 = pass size         | optional
# $2 = entropy pool size | optional
function mousePass () {
    passSize="8"
    entropyPoolSize="1K"
    if [ "${#}" -eq "1" ]; then
        passSize="${1}"
    fi
    if [ "${#}" -eq "2" ]; then
        passSize="${1}"
        entropyPoolSize="${2}"
    fi
    echo "Please move your mouse randomly." 1>&2
    echo "Generating password with ${passSize} digits from a ${entropyPoolSize} bytes entropy pool." 1>&2
    echo "Using date command as salt.\n" 1>&2
    echo $(echo $(echo $(date) $(sudo dd if=/dev/input/mice bs=1 count=${entropyPoolSize} 2>/dev/null)) | sha512sum | base64 | head -c ${passSize})
}
