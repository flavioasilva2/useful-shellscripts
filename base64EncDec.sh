#!/bin/bash

function b64enc () {
    if [ "${#}" -ne "1" ]; then
        echo "Número de argumentos incorreto, essa função aceita exatamente 1 argumento."
        return 1
    fi
    echo "$(printf "${1}" | base64)"
}

function b64dec () {
    if [ "${#}" -ne "1" ]; then
        echo "Número de argumentos incorreto, essa função aceita exatamente 1 argumento."
        return 1
    fi
    echo "$(printf ${1} | base64 -d)"
}
