#!/bin/bash

unset_java () {
        unset JAVA_HOME
        export PATH=$(echo $PATH | sed -e 's/\:\/opt\/jdk\/jdk-14\/bin//g' 2>/dev/null)
	export PATH=$(echo $PATH | sed -e 's/\:\/opt\/jdk\/jdk1.8.0_241\/bin//g' 2>/dev/null)
}

select_java14 () {
        unset_java
        export JAVA_HOME="/opt/jdk/jdk-14"
        export PATH="${PATH}:${JAVA_HOME}/bin"
}

select_java8 () {
        unset_java
        export JAVA_HOME="/opt/jdk/jdk1.8.0_241"
        export PATH="${PATH}:${JAVA_HOME}/bin"
}
